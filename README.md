# audio_rtp_sniffer

Python script for sniffing rtp payload from pcap files or live interface

Command for running an audio:
```
play -t raw -r 8k -e u-law -b 8 -c 1 <audio file>
```