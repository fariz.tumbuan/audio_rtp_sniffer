#!/usr/bin/python3
import os
import datetime
class RtpStream():
    def __init__(self, raw_packet, filename=None):
        rtp_packet = self._parse_rtp_packet(raw_packet)
        #TODO: add additional information
        # self.src_mac = 0
        # self.src_ip = 0
        # self.src_port = 0
        # self.dest_mac = 0
        # self.dest_ip = 0
        # self.dest_port = 0
        self.setup_frame = rtp_packet["setup_frame"]
        self.payload_type = rtp_packet["payload_type"] #TODO: handle different payload types
        self.expected_seq = rtp_packet["seq_number"]
        self.sync_id = rtp_packet["sync_id"]
        if filename is None:
            try:
                os.mkdir("audio_files")
            except FileExistsError:
                pass
            self.filename = "audio_files/{}_{}".format(self.setup_frame, self.sync_id)
        else:
            self.filename = filename
        self.write_packet(raw_packet)
    
    def set_filename(self, new_filename):
        self.filename = new_filename

    def write_packet(self, raw_packet):
        if (self.check_is_next_seq(raw_packet)):
            rtp_packet = self._parse_rtp_packet(raw_packet)
            with open(self.filename, "ab") as f:
                f.write(rtp_packet["payload"])
            self.expected_seq += 1
        else:
            pass

    def check_is_next_seq(self, raw_packet):
        rtp_packet = self._parse_rtp_packet(raw_packet)
        # print("ssrc | {} : {}".format(self.sync_id, rtp_packet["sync_id"]))
        # print("expected_seq | {} : {}".format(self.expected_seq, rtp_packet["seq_number"]))
        # print("===")
        return (rtp_packet["sync_id"] == self.sync_id \
            and rtp_packet["seq_number"] == self.expected_seq)

    def _parse_rtp_packet(self, raw_packet):
        content = {}
        content["setup_frame"] = int(raw_packet.setup_frame)
        content["payload_type"] = int(raw_packet.p_type) #TODO: change into string name
        content["seq_number"] = int(raw_packet.seq)
        content["sync_id"] = raw_packet.ssrc
        content["payload"] = self._parse_rtp_payload(raw_packet.payload)
        return content
    
    def _parse_rtp_payload(self, payload):
        pl = payload.split(":")
        pl = " ".join(pl)
        return bytearray.fromhex(pl)
