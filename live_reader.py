#play -t raw -r 8k -e u-law -b 8 -c 1 "2019-08-18 21:55:58.160874_581012_0x07460998"
import binascii
import sys
import wave
from os import mkdir

from stream import RtpStream
import pyshark

if __name__ == "__main__":
    if len(sys.argv) > 2:
        print("Usage: python3 pcap_reader.py <interface; empty for any>")
        sys.exit()
    if len(sys.argv) == 2:
        cap = pyshark.LiveCapture(interface=sys.argv[1], display_filter="rtp")
    else:
        cap = pyshark.LiveCapture("any", display_filter="rtp")
    rtp_streams = {}
    i = 0

    # cap.set_debug(True)
    try:
        for c in cap.sniff_continuously():
            if (i % 1000 == 0):
                print("Frame", i)
            eth_frame = c
            if eth_frame.rtp and eth_frame.rtp.payload:
                setup_frame = eth_frame.rtp.setup_frame
                ssrc = eth_frame.rtp.ssrc
                if setup_frame not in rtp_streams:
                    rtp_streams[setup_frame] = {}
                if ssrc not in rtp_streams[setup_frame]:
                    rtp_streams[setup_frame][ssrc] = RtpStream(eth_frame.rtp)
                    rtp_streams[setup_frame][ssrc].write_packet(eth_frame.rtp)
                else:
                    rtp_streams[setup_frame][ssrc].write_packet(eth_frame.rtp)
            i += 1
    except KeyboardInterrupt:
        print("Exiting process")
